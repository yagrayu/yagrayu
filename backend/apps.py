import logging

import redis
from django.apps import AppConfig

from .messaging import rabbitmq_connect


class BackendConfig(AppConfig):
    name = "backend"

    def ready(self):
        # TODO:remove in production

        redis_client = redis.StrictRedis(host="redis")
        # redis_client.delete("commands")
        keys = redis_client.keys()
        for k in keys:
            redis_client.delete(k)
        # until here

        # FIXME:fix somewhere else and then here. do not touch otherwise
        # rabbitmq_connect()

        try:
            from .table_utils import start_game_if_enough_players
            from .models import Table, PlayingUser

            tables = Table.objects.all()
            for table in tables:
                start_game_if_enough_players(table)
        except:
            pass
