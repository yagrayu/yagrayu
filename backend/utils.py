from django.db.models import Func


class RoundFunc(Func):
    function = "ROUND"
    template = "%(function)s(%(expressions)s, 0)"
