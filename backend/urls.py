from django.urls import path

from .views import (  # inform_one_view,
    LeaderBoardList,
    RemovePlayingUserDetail,
    index_view,
    inform_all_view,
    join_table_view,
    list_tables_view,
    login_user_view,
    messaging_log_view,
    request_action_view,
    table_detail_view,
    get_available_messages_endpoint,
    response_endpoint,
)

app_name = "backend"
urlpatterns = [
    path("", index_view),
    # users
    path("users/login/<str:username>/", login_user_view, name="create_user"),
    # tables
    path("tables/", list_tables_view, name="list_tables"),
    path("table/<int:table_id>/", table_detail_view, name="table_detail"),
    path(
        "table/<int:table_id>/join/<str:username>/", join_table_view, name="join_table"
    ),
    path(
        "table/<int:table_id>/leave/<str:username>/",
        RemovePlayingUserDetail.as_view(),
        name="leave_table",
    ),
    # messaging
    path("inform/<int:table_id>/", inform_all_view),
    # path("inform_one/<int:table_id>/<str:username>/", inform_one_view),
    path("request_action/<int:table_id>/<str:username>/", request_action_view),
    path("log/", messaging_log_view),
    path("messages/<int:table_id>/<str:username>/", get_available_messages_endpoint),
    path("response/<int:table_id>/<str:username>/", response_endpoint),
    # leader board list
    path("leader_board/", LeaderBoardList.as_view()),
]
