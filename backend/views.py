import json
import multiprocessing
import random
import sys
import time
import traceback
from copy import deepcopy

import redis
from backend import urls
from backend.messaging import rabbitmq_publish, rabbitmq_rpc
from backend.models import HandHistory, PlayingUser, Table, User
from backend.serializers import UserSerializer
from django.contrib.auth import login, logout
from django.db.models import Avg, Count, Func
from django.forms import model_to_dict
from django.http import Http404, HttpResponse, JsonResponse
from django.template import Context, Template
from django.urls import get_resolver
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from yagrayu.users.models import User

from .table_utils import join_table, start_new_hand
from .utils import RoundFunc


class LeaderBoardList(APIView):
    def get(self, request):
        users = User.objects.all().order_by("-total_chip_count")[:64]
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


def index_view(request):
    existing_urls = [i.pattern for i in urls.urlpatterns]
    template = "<ul>{%for i in urls%}<li>{{i}}</li>{%endfor%}</ul>"
    return HttpResponse(
        Template(template).render(context=Context({"urls": existing_urls}))
    )


# def signup_user_view(request, username):
#     user = User.objects.create(username=username)
#     login(request, user, backend="django.contrib.auth.backends.ModelBackend")
#     return JsonResponse({"status": "ok"})


def login_user_view(request, username):
    user, _ = User.objects.get_or_create(username=username)
    login(request, user, backend="django.contrib.auth.backends.ModelBackend")
    return JsonResponse({"status": "ok"})


def list_tables_view(request):
    tables = list(
        Table.objects.annotate(players_count=Count("playinguser"))
        .annotate(average_stack=Avg("playinguser__chip_count"))
        .annotate(
            hands_per_hour=RoundFunc(Avg("playinguser__id"))
        )  # TODO: count hands_per_hour
        .all()
        .values("id", "table_type", "players_count", "average_stack", "hands_per_hour")
    )
    return JsonResponse(tables, safe=False)


def table_detail_view(request, table_id):
    table = Table.objects.get(id=table_id)
    table_dict = model_to_dict(table)
    table_dict["start_datetime"] = table.start_datetime
    table_dict["empty_seats_count"] = table.seats_count - table.playinguser_set.count()
    table_dict["players"] = [
        {
            "id": i.id,
            "username": i.user.username,
            "table_id": i.table.id,
            "position": i.position,
            "chip_count": i.chip_count,
        }
        for i in table.playinguser_set.all()
    ]
    return JsonResponse(table_dict, safe=False)


def join_table_view(request, table_id, username):
    user = User.objects.get(username=username)
    table = Table.objects.get(id=table_id)
    msg = join_table(user, table)
    return JsonResponse({"status": msg})


class RemovePlayingUserDetail(APIView):
    def get_object(self, db_table, object_id):
        try:
            return db_table.objects.get(id=object_id)
        except db_table.DoesNotExist:
            raise Http404

    def delete(self, request, table_id, username):
        table = self.get_object(Table, table_id)
        remove_player = table.playinguser_set.get(user__username=username)
        remove_player.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


def inform_all_view(request, table_id):
    # TODO: remove exchange from high level call
    # it should be something like send_info_to_all_players()
    data = json.loads(request.body)
    event = data["event"]

    if event == "preflop":
        print("preflopprefloppreflop")
        players = data["players"]
        print("player count =", len(players))
        for player in players:
            data_copy = deepcopy(data)

            table_players = data_copy["players"]
            cards = [p for p in table_players if p["username"] == player["username"]][
                0
            ]["cards"]

            data_copy["current"] = {"cards": cards, "position": player["position"]}
            for other_player in data_copy["players"]:
                other_player.pop("cards")

            print(f"send to {table_id} {player['username']} message={data_copy}")
            exchange = f"player_queue_{table_id}_{player['username']}"
            rabbitmq_publish(data_copy, exchange)

        return JsonResponse({"status": "ok"})

    if event in ("flop_cards", "turn_card", "river_card"):
        table = Table.objects.get(id=table_id)
        players = table.playinguser_set.all()
        for player in players:
            exchange = f"player_queue_{table_id}_{player.username}"
            rabbitmq_publish(data, exchange)

        return JsonResponse({"status": "ok"})

    if event == "winner":
        table = Table.objects.get(id=table_id)
        players = table.playinguser_set.all()
        for player in players:
            exchange = f"player_queue_{table_id}_{player.username}"
            rabbitmq_publish(data, exchange)

        return JsonResponse({"status": "ok"})

    if event == "save":
        table = Table.objects.get(id=table_id)
        HandHistory.objects.create(history=data["data"])

        # FIXME: uncomment when works on one hand
        # start_new_hand(table)

        return JsonResponse({"status": "ok"})

    return JsonResponse({"status": "ok"})


@csrf_exempt
def request_action_view(request, table_id, username):
    action_space_json = request.POST.get("action_space")

    request_exchange = f"player_queue_{table_id}_{username}"
    response_exchange = f"response_queue_{table_id}_{username}"

    try:
        result = rabbitmq_rpc(
            action_space_json, request_exchange, response_exchange, timeout=10
        )
        # result = {"status": "ok", "action": {"type": "fold"}}
        return JsonResponse(result, safe=False)
    except Exception as e:
        return JsonResponse(
            {
                "status": "error",
                "description": str(sys.exc_info()),
                "traceback": traceback.format_exc(),
            }
        )


def messaging_log_view(request):
    redis_client = redis.StrictRedis(host="redis")

    if redis_client.exists("commands"):
        commands = json.loads(redis_client.get("commands"))
    else:
        commands = []
    return JsonResponse(commands, safe=False)


def get_available_messages_endpoint(request, table_id, username):
    redis_client = redis.StrictRedis(host="redis")

    routing_key = f"player_queue_{table_id}_{username}"
    if redis_client.exists(routing_key):
        messages = json.loads(redis_client.get(routing_key))
    else:
        messages = []
    redis_client.set(routing_key, json.dumps([]))
    return JsonResponse(messages, safe=False)


@csrf_exempt
def response_endpoint(request, table_id, username):
    redis_client = redis.StrictRedis(host="redis")

    response_exchange = f"response_queue_{table_id}_{username}"

    if redis_client.exists(response_exchange):
        response = json.loads(redis_client.get(response_exchange))
    else:
        response = json.loads({})

    rabbitmq_publish(response, response_exchange)

    return JsonResponse({"status": "ok"})

