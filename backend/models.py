import os
import uuid

from django.core.files import File
from django.db import models
from django.dispatch import receiver

from yagrayu.users.models import User


class Table(models.Model):
    TABLE_TYPE_CHOICES = [("Cash", "Cash"), ("MTT", "MTT"), ("SNG", "SNG")]

    table_type = models.CharField(max_length=64, choices=TABLE_TYPE_CHOICES)
    start_datetime = models.DateTimeField("start time", auto_now_add=True)
    end_datetime = models.DateTimeField("end time", null=True, blank=True)
    button_position = models.PositiveSmallIntegerField(
        verbose_name="Button position", default=1
    )  # to redis
    small_blind = models.IntegerField()
    big_blind = models.IntegerField()
    ante = models.IntegerField(default=0)
    seats_count = models.IntegerField(default=9)

    class Meta:
        verbose_name = "table"
        verbose_name_plural = "tables"

    def __str__(self):
        return f"{self.table_type}"

    @property
    def table_info(self):
        players = [
            {"position": i.position, "username": i.username, "stack_size": i.chip_count}
            for i in self.playinguser_set.all()
        ]
        return {
            "id": self.id,
            "blinds": {
                "small": self.small_blind,
                "big": self.big_blind,
                "ante": self.ante,
            },
            "players": players,
            "button_position": self.button_position,
        }


class PlayingUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.SET_NULL, null=True)
    position = models.SmallIntegerField()
    chip_count = models.PositiveIntegerField(verbose_name="Chips")  # to redis?

    class Meta:
        verbose_name = "Playing user"
        unique_together = ["table", "user"]

    def __str__(self):
        return f"{self.user} {self.chip_count} on {self.table}"

    @property
    def username(self):
        return self.user.username


class Note(models.Model):
    player = models.ForeignKey(User, on_delete=models.CASCADE)
    note = models.TextField(max_length=256)

    class Meta:
        verbose_name = "note"
        verbose_name_plural = "notes"


class HandHistory(models.Model):
    table = models.ForeignKey(Table, on_delete=models.SET_NULL, null=True)
    history = models.TextField(default="")  # json
    start_datetime = models.DateTimeField("Start time", null=True)
    end_datetime = models.DateTimeField("End time", null=True)
