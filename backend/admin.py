from django.contrib import admin

from .models import PlayingUser, Table


class PlayingUserInline(admin.TabularInline):
    model = PlayingUser


class TableAdmin(admin.ModelAdmin):
    inlines = [PlayingUserInline]
    list_display = ["id", "table_type"]


admin.site.register(Table, TableAdmin)
admin.site.register(PlayingUser)
