# TODO: if rabbitmq will not work,
# use redis pubsub
# https://medium.com/@johngrant/python-redis-pub-sub-6e26b483b3f7

import json
import logging
import multiprocessing
import os
import time
import uuid

import pika
import redis

RABBITMQ_URL = os.environ.get("RABBITMQ_URL", "amqp://guest:guest@localhost:5672/%2F")
connection = None
channel = None
redis_client = redis.StrictRedis(host="redis")


def rabbitmq_connect():
    # TODO:
    while True:
        try:
            # global connection
            connection = pika.BlockingConnection(pika.URLParameters(RABBITMQ_URL))
            # global channel
            # channel = connection.channel()

            return connection
        except:
            logging.error("waiting for RabbitMQ to become available...")
            time.sleep(3)


import asyncio
import websockets


async def websockets_send_internal(message, routing_key):
    uri = "ws://localhost:8765"
    async with websockets.connect(uri) as websocket:
        await websocket.send(
            json.dumps({"routing_key": routing_key, "message": message})
        )
        print(f'> {json.dumps({"routing_key": routing_key, "message": message})}')


import itertools


def websockets_send(message, routing_key):
    args = [message, routing_key]
    tasks = itertools.starmap(websockets_send_internal, args)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(tasks))
    loop.close()


def rabbitmq_publish(data, exchange):
    # global channel
    connection = rabbitmq_connect()  # FIXME: this should not be here
    # but it does not work with global variable for some reason
    channel = connection.channel()

    channel.exchange_declare(exchange=exchange, exchange_type="fanout")

    channel.basic_publish(exchange=exchange, routing_key="", body=json.dumps(data))
    logging.info(f"publish message={data} to exchange={exchange}")

    log_message(exchange, data)

    ### short polling
    global redis_client
    if redis_client.exists(exchange):
        messages = json.loads(redis_client.get(exchange))
    else:
        messages = []
    messages.append(data)
    redis_client.set(exchange, json.dumps(messages))


def log_message(exchange, data):
    global redis_client

    if redis_client.exists("commands"):
        commands = json.loads(redis_client.get("commands"))
    else:
        commands = []

    commands.append({"exchange": exchange, "message": data})
    redis_client.set("commands", json.dumps(commands))


def rabbitmq_rpc(action_space_json, request_exchange, response_exchange, timeout=30):
    message = {"event": "request_action", "action_space": json.loads(action_space_json)}

    global redis_client
    if redis_client.exists(request_exchange):
        messages = json.loads(redis_client.get(request_exchange))
    else:
        messages = []
    messages.append(message)
    redis_client.set(request_exchange, json.dumps(messages))

    # TODO:result endpoint
    # result = {"status": "timeout"}

    log_message(request_exchange, message)

    rpc_client = RpcClient(response_exchange, timeout=timeout)
    result = rpc_client.call(request_exchange, message)
    return result


class RpcClient:
    def __init__(self, response_exchange, timeout=30):
        self.response_exchange = response_exchange

        # global channel
        connection = rabbitmq_connect()  # FIXME: this should not be here
        # but it does not work with global variable for some reason
        channel = connection.channel()

        result_queue = channel.queue_declare(queue=self.response_exchange)
        self.callback_queue = result_queue.method.queue
        self.timeout = timeout

        channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True,
        )

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            logging.info(
                f"receive response on exchange={self.response_exchange} message={body}"
            )
            self.response = json.loads(body)

    def call(self, request_exchange, data):
        self.response = None
        self.corr_id = str(uuid.uuid4())

        connection = rabbitmq_connect()
        channel = connection.channel()
        channel.exchange_declare(exchange=request_exchange, exchange_type="fanout")
        channel.basic_publish(
            exchange=request_exchange,
            routing_key="",
            properties=pika.BasicProperties(
                reply_to=self.callback_queue, correlation_id=self.corr_id
            ),
            body=json.dumps(data),
        )
        logging.info(f"request action message={data} to exchange={request_exchange}")
        log_message(request_exchange, data)

        # global connection

        start_time = time.time()
        while self.response is None:
            connection.process_data_events()

            now_time = time.time()
            if now_time - start_time > self.timeout:
                self.response = {"status": "timeout"}
                break

        return self.response
