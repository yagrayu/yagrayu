from backend.models import PlayingUser, Table
from backend.table_utils import join_table

from yagrayu.users.models import User

table = Table.objects.create(table_type="Cash", small_blind=10, big_blind=20)

usernames = ["Daria", "Alexandr", "Max", "Valery"]

for i, username in enumerate(usernames):
    user, _ = User.objects.get_or_create(username=username)
    join_table(user, table)
