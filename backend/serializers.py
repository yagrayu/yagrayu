from backend.models import PlayingUser, Table
from rest_framework import serializers

from yagrayu.users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("name", "nationality", "email", "total_chip_count")


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table


class PlayingUserSerializer(serializers.ModelSerializer):
    table = TableSerializer(read_only=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = PlayingUser
        fields = ("table", "user", "position", "chip_count")
