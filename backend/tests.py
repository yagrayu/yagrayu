import json

from django.contrib import auth
from django.test import Client, TestCase

from yagrayu.users.models import User

from .models import PlayingUser, Table


class APITest(TestCase):
    def setUp(self):
        self.client = Client()
        self.username = "player_test"

    # def test_signup_user(self):
    #     self.client.post(f"/api/users/signup/{self.username}/")
    #     user = User.objects.get(username=self.username)
    #     self.assertEqual(user.username, self.username)

    def test_login_user(self):
        user = User.objects.create(username=self.username)
        response = self.client.post(f"/api/users/login/{self.username}/")

        loggedin_user = auth.get_user(self.client)
        self.assertEqual(loggedin_user.username, self.username)

    def test_join_and_leave_table(self):
        user1 = User.objects.create(username=self.username)
        self.client.post(f"/api/users/login/{user1.username}/")

        user2 = User.objects.create(username="Isildur1")
        user3 = User.objects.create(username="kidpoker")

        table = Table.objects.create(table_type="Cash", small_blind=10, big_blind=20)
        self.client.post(f"/api/table/{table.id}/join/{user1.username}/")
        self.client.post(f"/api/table/{table.id}/join/{user2.username}/")
        self.client.post(f"/api/table/{table.id}/join/{user3.username}/")

        loggedin_user = auth.get_user(self.client)
        self.assertEqual(loggedin_user.username, user1.username)
        self.assertEqual(table.playinguser_set.count(), 3)

        self.client.delete(f"/api/table/{table.id}/leave/{user1.username}/")
        self.assertEqual(table.playinguser_set.count(), 2)

    def test_list_tables(self):
        user1 = User.objects.create(username="1")
        user2 = User.objects.create(username="2")
        user3 = User.objects.create(username="3")
        user4 = User.objects.create(username="4")

        table1 = Table.objects.create(table_type="Cash", small_blind=10, big_blind=20)
        table2 = Table.objects.create(
            table_type="Cash", small_blind=20, big_blind=40, ante=4
        )

        self.client.post(f"/api/table/{table1.id}/join/{user1.username}/")
        self.client.post(f"/api/table/{table1.id}/join/{user3.username}/")
        self.client.post(f"/api/table/{table2.id}/join/{user2.username}/")
        self.client.post(f"/api/table/{table2.id}/join/{user4.username}/")

        self.assertEqual(table1.playinguser_set.count(), 2)
        self.assertEqual(table2.playinguser_set.count(), 2)

        response = self.client.get(f"/api/tables/")
        content = json.loads(response.content)
        assert len(content), 2
