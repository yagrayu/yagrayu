import json
import logging
import random

from django.http import Http404

from .messaging import rabbitmq_publish
from .models import PlayingUser


def join_table(user, table):
    if table.playinguser_set.filter(user__username=user.username):
        return "player already at the table"

    busy_seats = list(table.playinguser_set.all().values_list("position", flat=True))
    empty_seats = list(set(range(1, table.seats_count + 1)).difference(busy_seats))

    if not empty_seats:
        return "no empty seats"

    position = random.choice(empty_seats)
    chip_count = 10000  # TODO:

    PlayingUser.objects.create(
        user=user, table=table, position=position, chip_count=chip_count
    )

    start_game_if_enough_players(table)
    return "ok"


def start_game_if_enough_players(table):
    players_count = table.playinguser_set.count()

    if players_count > 1:
        logging.info(f"start game on table {table.id}")
        start_new_hand(table)


def start_new_hand(table):
    rabbitmq_publish(table.table_info, exchange="start_hand")
