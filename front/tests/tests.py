from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver

class MySeleniumTests(StaticLiveServerTestCase):
    fixtures = ['user-data.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_public_login(self):
        self.selenium('%s%s' % (self.live_server_url, '/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('test_user')
        self.selenium.find_element_by_xpath('//input[@value="Select table"]').click()

    def test_private_table_login(self):
        self.selenium('%s%s' % (self.live_server_url, '/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('playing_user')
        id_input = self.selenium.find_element_by_name("tableId")
        id_input.send_keys('1')
        self.selenium.find_element_by_xpath('//input[@value="Join Table"]').click()
