import React, {Component} from "react";
import ReactDOM from "react-dom";
import Root from "./components/Root";

const wrapper = document.getElementById("root");
wrapper ? ReactDOM.render(<Root/>, wrapper) : false;
