import React, {Component} from "react";
import styles from "./table-style.css"
import Card from "../Card";
import Player from "../Player";
import Panel from "../ActionPanel";
import axios from 'axios';

class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], // consider this is data taken
            actions: [
                {"type": "fold"},
                {"type": "check"},
                {"type": "call", "size": 20},
                {"type": "raise", "min": 50, "max": 150}
            ],
            actionPosition: 4,
            cardsOnTable: [], // add cards here from flop, turn, river events
            event: '',
            players: [],    // taken from preflop
            currentPlayerPosition: 0, // taken from preflop
            currentPlayerCards: [],    // taken from preflop
            isNewHand: false,
            handleState: 0,
            call: ''
        };
        this.username = this.props.location.state.username; //might cause some shit
        this.tableId = this.props.location.state.tableId;
        this.handleRiver = this.handleRiver.bind(this);
        this.handleTurn = this.handleTurn.bind(this);
        this.handleFlop = this.handleFlop.bind(this);
        this.handleActions = this.handleActions.bind(this);
        this.processEvents = this.processEvents.bind(this);
        this.getData = this.getData.bind(this);
        this.setCall = this.setCall.bind(this);

    }

    setCall(value) {
        this.setState({call: value});
        console.log(this.state.call)
    };

    handleTurn(record) {
        this.setState({
            event: record.event,
            cardsOnTable: [...this.state.cardsOnTable, record.turn_card]
        });
        console.log(this.state.event);
    }

    handleRiver(record) {
        this.setState({
            event: record.event,
            cardsOnTable: [...this.state.cardsOnTable, record.river_card]
        });
        console.log(this.state.event);
    }

    handleFlop(record) {
        this.setState({
            event: record.event,
            cardsOnTable: record.flop_cards
        });
        console.log(this.state.event);
    }

    handleActions(record) {
        this.setState({
            event: record.event,
            actions: record.current.action_space,
            actionPosition: record.current.position
        });
        console.log(this.state.event);
    }

    processEvents(response) {
        try {
            response.map((record) => {
                if (record.event === 'preflop') {
                    this.setState({
                        event: record.event,
                        players: record.players,
                        currentPlayerPosition: record.current.position,
                        currentPlayerCards: record.current.cards
                    });
                    console.log(this.state.event);
                }

                if (record.event === 'flop_cards') {
                    setTimeout(() => {
                        this.handleFlop(record)
                    }, 2000);
                }

                if (record.event === 'turn_card') {
                    setTimeout(() => {
                        this.handleTurn(record)
                    }, 4000);
                }

                if (record.event === 'river_card') {
                    setTimeout(() => {
                        this.handleRiver(record)
                    }, 6000);
                }

                if (record.event === 'request_action') {
                    this.handleActions(record)
                }
            });
        } catch (error) {
            console.error(error)
        }
    };

    componentDidMount() {
        this.getData();
        this.interval = setInterval(() => {
            this.getData();
        }, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    getData() {
        console.log(this.username, this.tableId);
        let handleState = this.state.handleState;
        axios.get(`http://localhost:8000/api/messages/${this.tableId}/${this.username}/`)
            .then(response => {
                this.processEvents(response.data.slice(handleState, handleState + 5));
                this.setState({handleState: handleState + 5});
            })
            .catch(console.log)
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.containerTable}>
                    <div className={styles.table}>
                        {
                            this.state.event !== "preflop" && this.state.event !== '' &&
                            <div className={styles.cardsArea}>
                                {
                                    this.state.cardsOnTable.map((card, index) => (
                                        <Card value={card[0]}
                                              suit={card[1]}
                                              key={index}
                                        />
                                    ))
                                }
                            </div>
                        }

                        <div className={styles.playerArea}>
                            {
                                this.state.players.map((player, index) => (
                                    <Player player={player}
                                            currentPlayerPosition={this.state.currentPlayerPosition}
                                            currentPlayerCards={this.state.currentPlayerCards}
                                            position={player.position}
                                            key={player.position}
                                            value={this.state.call}
                                    />
                                ))
                            }
                        </div>
                    </div>
                </div>
                <div className={styles.panel}>
                    {
                        this.state.event === 'request_action' &&
                        this.state.currentPlayerPosition === this.state.actionPosition
                        &&
                        <Panel
                            actions={this.state.actions}
                            tableId={this.tableId}
                            username={this.username}
                            setCall={this.setCall}
                        />
                    }
                </div>
            </div>

        )
    }
}

export default Table;

