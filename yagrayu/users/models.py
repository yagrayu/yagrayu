import os
import uuid

from django.contrib.auth.models import AbstractUser
from django.core.files import File
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


def file_extension(filename):
    return filename.split(".")[-1]


def get_avatar_upload_path(filename):
    return os.path.join("avatars", "%s.%s" % (uuid.uuid4(), file_extension(filename)))


def get_country_upload_path(filename):
    return os.path.join("countries", "%s.%s" % (uuid.uuid4(), file_extension(filename)))


class Country(models.Model):
    name = models.CharField(max_length=256)
    # flag = models.ImageField(
    #     upload_to=get_country_upload_path("FIXME:tut sho?"),
    #     null=True,
    #     blank=True,
    #     verbose_name="Flag",
    # )

    class Meta:
        verbose_name = "country"

    def __str__(self):
        return self.name


class User(AbstractUser):
    # name and username are different fields. username is from parent AbstractUser
    # name = models.CharField(_("Name of User"), blank=True, max_length=255)
    nationality = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)
    # note = models.ForeignKey("backend.Note", on_delete=models.CASCADE)
    # hands = models.ForeignKey("backend.HandHistory", on_delete=models.CASCADE)
    # avatar = models.ImageField(
    #     upload_to=get_avatar_upload_path("FIXME: tut sho?"),
    #     verbose_name="Avatar",
    #     null=True,
    # )
    email = models.EmailField(max_length=255, verbose_name="E-mail")
    total_chip_count = models.PositiveIntegerField(
        verbose_name="Total chip count", default=0
    )

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})

    def __str__(self):
        return self.username


@receiver(models.signals.post_delete, sender=User)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
        Deletes file from filesystem
        when corresponding `File` object is deleted.
    """
    if instance.avatar:
        if os.path.isfile(instance.avatar.path):
            os.remove(instance.avatar.path)


@receiver(models.signals.post_delete, sender=User)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
        Deletes old file from filesystem
        when corresponding `File` object is updated
        with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = User.objects.get(pk=instance.pk).avatar
    except File.DoesNotExist:
        return False

    new_file = instance.avatar
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


@receiver(models.signals.post_delete, sender=Country)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
        Deletes file from filesystem
        when corresponding `File` object is deleted.
    """
    if instance.flag:
        if os.path.isfile(instance.flag.path):
            os.remove(instance.flag.path)


@receiver(models.signals.post_delete, sender=Country)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
        Deletes old file from filesystem
        when corresponding `File` object is updated
        with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = Country.objects.get(pk=instance.pk).flag
    except File.DoesNotExist:
        return False

    new_file = instance.flag
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
