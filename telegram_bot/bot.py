import pika
import telebot
from telebot import types
import config
from dotenv import load_dotenv
import os
from config import *
import urllib.request, json
import redis
import redis_worker
from threading import Timer, Lock
from time import sleep
import redis_lock

load_dotenv()

bot = telebot.TeleBot(os.environ["TELEGRAM_TOKEN"])

exchange_name = ""
queue_name = ""
response_queue_name = ""
timers = dict()

redis_pool = redis.ConnectionPool(host='localhost', port=6379, db=0)

def start_queue(message):
	queue_name =  redis_worker.get_prop(redis_pool, message.chat.id, "queue_name")
	# try:
	connection = pika.BlockingConnection(
		pika.ConnectionParameters(host='localhost'))
	channel = connection.channel()
	redis_worker.set_prop(redis_pool, message.chat.id, connection, "connection")

	exchange_name = redis_worker.get_prop(redis_pool, message.chat.id, "exchange_name")
	channel.exchange_declare(exchange=exchange_name, exchange_type='fanout')

	result = channel.queue_declare(queue=queue_name, exclusive=True)
	queue_name = result.method.queue

	channel.queue_bind(exchange=exchange_name, queue=queue_name)

	print(' [*] Waiting for logs. To exit press CTRL+C')

	def callback(ch, method, properties, body):
		data = json.loads(body)
		if data["event"] == 'preflop':
			current_balance = data["players"][int(data['current']['position'])]["stack"]
			redis_worker.set_prop(redis_pool, message.chat.id, current_balance, "current_balance")
			current_cards = prettify_cards(data['current']['cards'])
			bot.send_message(message.chat.id, "Your cards: " + current_cards)
			redis_worker.set_prop(redis_pool, message.chat.id, current_cards, "current_cards")
		elif data["event"] == 'flop_cards':
			for card in data["flop_cards"]:
				card_stack = prettify_cards(card + " ")
				redis_worker.set_prop(redis_pool, message.chat.id, card_stack, "card_stack")
			bot.send_message(message.chat.id, "Card stack:\n" + card_stack)
		elif data["event"] == 'turn_card':
			card_stack = redis_worker.get_prop(redis_pool, message.chat.id, "card_stack")
			card_stack += prettify_cards(data["turn_card"] + " ")
			bot.send_message(message.chat.id, "Card stack:\n" + card_stack)
			redis_worker.set_prop(redis_pool, message.chat.id, card_stack, "card_stack")
		elif data["event"] == 'river_card':
			card_stack = redis_worker.get_prop(redis_pool, message.chat.id, "card_stack")
			card_stack += prettify_cards(data["river_card"])
			bot.send_message(message.chat.id, "Card stack:\n" + card_stack)
			redis_worker.set_prop(redis_pool, message.chat.id, card_stack, "card_stack")
		elif data["event"] == 'winner':	
			current_balance = redis_worker.get_prop(redis_pool, message.chat.id, "current_balance")
			if current_balance is None:
				current_balance = 0
			current_balance += int(data["pot"])
			bot.send_message(message.chat.id, "You win:\n" + str(data["pot"]) + ".\nCurr balance:" + str(current_balance))
			card_stack = ""
			current_cards = ""
			redis_worker.set_prop(redis_pool, message.chat.id, card_stack, "card_stack")
			redis_worker.set_prop(redis_pool, message.chat.id, current_balance, "current_balance")
			redis_worker.set_prop(redis_pool, message.chat.id, current_cards, "current_cards")
		elif data["event"] == 'lose':
			current_cards  = ""
			card_stack = ""
			oponents_cards = data["oponents_cards"]
			name = ""
			cards = ""
			oponents_data = ""
			for oponent_cards in oponents_cards:
				name = oponent_cards['name']
				cards = oponent_cards['cards']
				oponents_data += name + ". Cards:" + oponent_cards['cards'] + "\n"
			oponents_data = oponents_data[:-1]
			bot.send_message(message.chat.id, "You lose this round\n" + oponents_data)
			redis_worker.set_prop(redis_pool, message.chat.id, card_stack, "card_stack")
			redis_worker.set_prop(redis_pool, message.chat.id, current_cards, "current_cards")
		elif data["event"] == 'request_action':
			bot.send_message(message.chat.id, "You have 10 sec to make move!")
			keyboard = types.InlineKeyboardMarkup(row_width=1)
			for action in data['action_space']:
				if action['type'] == "call":
					call_size = data['action_space'][2]["size"]
					keyboard.add(types.InlineKeyboardButton(text="Call", callback_data="call"))
					bot.send_message(message.chat.id, "Call: " + str(call_size))
				elif action['type'] == "fold":
					keyboard.add(types.InlineKeyboardButton(text="Fold", callback_data="fold"))
				elif action['type'] == "raise":	
					min_size = data['action_space'][3]["min"]
					max_size = data['action_space'][3]["max"]
					redis_worker.set_prop(redis_pool, message.chat.id, min_size, "min_size")
					redis_worker.set_prop(redis_pool, message.chat.id, max_size, "max_size")
					bot.send_message(message.chat.id, "Bet: min: " + str(min_size) + " max: " + str(max_size))
					keyboard.add(types.InlineKeyboardButton(text="Raise", callback_data="raise"))
				elif action['type'] == "check":		
					keyboard.add(types.InlineKeyboardButton(text="Check", callback_data="check"))
			move_message = bot.send_message(message.chat.id, "Make move:", reply_markup=keyboard)
			redis_worker.set_user_state(redis_pool, message.chat.id, config.States.ACTION.value)
			
			timers[message.chat.id] = Timer(10.0, action_timeout, [message.chat.id, move_message.message_id, move_message.text])
			timers[message.chat.id].start()
			timers[message.chat.id].join()

	channel.basic_consume(
		queue=queue_name, on_message_callback=callback, auto_ack=True)

	channel.start_consuming()
	# except:
	# 	bot.send_message(message.chat.id, "Table is expired. Try again")

def action_timeout(chat_id, message_id, text):
	lock = redis_lock.Lock(conn, str(chat_id))
	if lock.acquire(blocking=False):
		if timers[message.chat.id].is_alive():
			timers[message.chat.id].cancel()
			bot.send_message(chat_id, "Timeout")
			bot.edit_message_text(chat_id=chat_id, message_id=message_id, text=text)
			message  = {"status": "timeout"}
			send_to_queue(message)
		lock.release()

def prettify_cards(cards):
	return cards.replace("h", "\u2665").replace("s", "\u2660").replace("c", "\u2663").replace("d", "\u2666") 

def send_to_queue(message):
	response_queue_name = redis_worker.get_prop(redis_pool, message.chat.id, "response_queue_name")
	connection = redis_worker.get_prop(redis_pool, message.chat.id, "connection")
	connection = pika.BlockingConnection(
		pika.ConnectionParameters(host='localhost'))
	channel = connection.channel()
	channel.basic_publish(exchange=response_queue_name, routing_key=response_queue_name , body=json.dumps(message))

@bot.message_handler(commands=['start'])
def start(message):
	global debug
	if debug == False:
		bot.send_message(message.chat.id, "Started. Enter /login or /sign_up")
	else:
		exchange_name = "player_queue_999_Max"
		queue_name = "player_queue_999_Max"
		response_queue_name = "response_queue_999_Max"
		redis_worker.set_prop(redis_pool, message.chat.id, exchange_name, "exchange_name")
		redis_worker.set_prop(redis_pool, message.chat.id, queue_name, "queue_name")
		redis_worker.set_prop(redis_pool, message.chat.id, response_queue_name, "response_queue_name")
		start_queue(message)

@bot.message_handler(commands=['login'])
def login(message):
	bot.send_message(message.chat.id, "Enter your name for login")
	redis_worker.set_user_state(redis_pool, message.chat.id, config.States.LOGIN.value)

@bot.message_handler(commands=['sign_up'])
def sign_up(message):
	bot.send_message(message.chat.id, "Enter your name for signup")
	redis_worker.set_user_state(redis_pool, message.chat.id, config.States.SIGNUP.value)

@bot.message_handler(func=lambda message: redis_worker.get_user_state(redis_pool, message.chat.id) == \
	config.States.LOGIN.value)
def auth_login(message):
	player_name = message.text
	try:
		with urllib.request.urlopen(os.environ["API_PATH"] + "/api/users/login/" + player_name) as url:
			data = json.loads(url.read().decode())
			if data['status'] == "ok":
				bot.send_message(message.chat.id, "Welcome " + player_name)
				redis_worker.set_prop(redis_pool, message.chat.id, player_name, "player_name")
				redis_worker.set_user_state(redis_pool, message.chat.id, config.States.INITIAL.value)
			elif data['status'] == "exists":
				bot.send_message(message.chat.id, "This name already exists. Try again")
	except:
		bot.send_message(message.chat.id, "Wrong login")
		player_name = ""

@bot.message_handler(func=lambda message: redis_worker.get_user_state(redis_pool, message.chat.id) == \
	config.States.SIGNUP.value)
def auth_signup(message):
	player_name = message.text
	try:
		with urllib.request.urlopen(os.environ["API_PATH"] + "/api/users/signup/" + player_name) as url:
			data = json.loads(url.read().decode())
			if data['status'] == "ok":
				bot.send_message(message.chat.id, "Welcome " + player_name)
				redis_worker.set_prop(redis_pool, message.chat.id, player_name, "player_name")
				redis_worker.set_user_state(redis_pool, message.chat.id, config.States.INITIAL.value)
	except:
		bot.send_message(message.chat.id, "Cant sign up")
		player_name = ""

@bot.message_handler(commands=['get_tables'])
def tables(message):
	player_name = redis_worker.get_prop(redis_pool, message.chat.id, "player_name")
	if player_name == "":
		bot.send_message(message.chat.id, "Please /login firstly to show tables or /signup")
	else:
		with urllib.request.urlopen(os.environ["API_PATH"] + "/api/tables/") as url:
			tables = json.loads(url.read().decode())
			tables_str = ""
			for table in tables:
				tables_str += "/" + str(table['id']) + "\n"
			bot.send_message(message.chat.id, 'Active tables:\n' + tables_str)
			redis_worker.set_user_state(redis_pool, message.chat.id, config.States.TABLE_JOIN.value)

@bot.message_handler(func=lambda message: redis_worker.get_user_state(redis_pool, message.chat.id) == \
	config.States.TABLE_JOIN.value)
def table_join(message):
	table_id = message.text[1:]
	try:
		player_name = redis_worker.get_prop(redis_pool, message.chat.id, "player_name")
		print(player_name)
		with urllib.request.urlopen(os.environ["API_PATH"] + "/api/table/" + \
			table_id + "/join/" + player_name) as url:
			data = json.loads(url.read().decode())
			if data['status'] == "ok":
				bot.send_message(message.chat.id, "Successfully joined table #" + table_id)
				exchange_name = "player_queue_" + table_id + "_" + player_name
				response_queue_name = "response_queue_" + table_id + "_" + player_name
				redis_worker.set_prop(redis_pool, message.chat.id, exchange_name, "queue_name")
				redis_worker.set_prop(redis_pool, message.chat.id, exchange_name, "exchange_name")
				redis_worker.set_prop(redis_pool, message.chat.id, response_queue_name, "response_queue_name")
				start_queue(message) 
			else:
				bot.send_message(message.chat.id, "Cant joint to table. Try Again. Or try /get_tables one more time")
	except:
		bot.send_message(message.chat.id, "Cant join this table")

@bot.message_handler(commands=['reset', 'exit'])
def reset(message):
	connection = redis_worker.get_prop(redis_pool, message.chat.id, "connection")
	connection.close()
	bot.send_message(message.chat.id, "closed")

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
	while timers[message.chat.id] is None:
		sleep(0.1)
	is_timeout = False
	lock = redis_lock.Lock(conn, str(chat_id))
	if lock.acquire(blocking=False):
		if timers[message.chat.id].is_alive():
			timers[message.chat.id].cancel()
		else:
			is_timeout = True
		lock.release()
	else:
		return
	if is_timeout:
		return
	if redis_worker.get_user_state(redis_pool, call.message.chat.id) == config.States.ACTION.value:
		redis_worker.set_user_state(redis_pool, call.message.chat.id, config.States.INITIAL.value)
		bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=call.message.text)
		if call.data == "raise":
			bot.send_message(call.message.chat.id, "Raise")
			bot.send_message(call.message.chat.id, "Enter stack:")
			redis_worker.set_prop(redis_pool, message.chat.id, True, "raise_move")
			redis_worker.set_user_state(redis_pool, message.chat.id, config.States.TEXT.value)
		elif call.data == "call":
			bot.send_message(call.message.chat.id, "Call")
			message  = {"status": "ok", "action": {"type": "call"}}
			send_to_queue(message)
		elif call.data == "fold":
			bot.send_message(call.message.chat.id, "Fold")
			message  = {"status": "ok", "action": {"type": "fold"}}
			send_to_queue(message)
			redis_worker.set_prop(redis_pool, message.chat.id, current_cards, "current_cards")
		elif call.data == "check":
			bot.send_message(call.message.chat.id, "Check")
			message  = {"status": "ok", "action": {"type": "check"}}
		send_to_queue(message)
	else:
		bot.send_message(call.message.chat.id, "Its not your move now")

@bot.message_handler(func=lambda message: redis_worker.get_user_state(redis_pool, message.chat.id) == \
	config.States.TEXT.value)
def any_msg(message):
	try:
		min_size = redis_worker.get_prop(redis_pool, message.chat.id, "min_size")
		max_size = redis_worker.get_prop(redis_pool, message.chat.id, "max_size")
		if redis_worker.get_prop(redis_pool, message.chat.id, "raise_move"):

			bet_value = message.text 
			if not bet_value.isdigit():
				bot.send_message(message.chat.id, "Not a digit")
			elif int(bet_value) > max_size or int(bet_value) < min_size:
				bot.send_message(message.chat.id, "Not in interval " + min_size + ":" + max_size)
			else:
				message = {"status": "ok", "action": {"type": "raise", "size": bet_value}}
				send_to_queue(message)
				current_balance = redis_worker.get_prop(redis_pool, message.chat.id, "current_balance")
				current_balance -= int(bet_value)
				redis_worker.set_prop(redis_pool, message.chat.id, False, "raise_move")
				redis_worker.set_prop(redis_pool, message.chat.id, current_balance, "current_balance")
	except:
		bot.send_message(message.chat.id, "Error in bet operation")

@bot.message_handler(comands=["get_bal"])
def get_bal(message):
	current_balance = redis_worker.get_prop(redis_pool, message.chat.id, "current_balance")
	current_cards = redis_worker.set_prop(redis_pool, message.chat.id, current_cards, "current_cards")
	bot.send_message(message.chat.id, "Stack: " + str(current_balance))
	if current_cards != "":
		bot.send_message(message.chat.id, "Your cards now: " + current_cards)

@bot.message_handler(comands=["get_cards"])
def get_cards(message):
	current_cards = redis_worker.get_prop(redis_pool, message.chat.id, "current_cards")
	card_stack = redis_worker.get_prop(redis_pool, message.chat.id, "card_stack")
	if current_cards != "":
		bot.send_message(message.chat.id, "Your cards now: " + current_cards)
	if card_stack == "":
		bot.send_message(message.chat.id, "Preflop. Table is emplty")
	else:
		bot.send_message(message.chat.id, "Table" + prettify_cards(card_stack))

if __name__ == "__main__":
	bot.polling(none_stop=True)