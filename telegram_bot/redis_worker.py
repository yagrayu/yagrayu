import redis
import config 

def get_user_state(pool, user_id):
    r = redis.Redis(connection_pool=pool)

    value = r.get(user_id)
    if value is None:
        return config.States.INITIAL.value

    return value.decode()

def set_user_state(pool, user_id, value):
    r = redis.Redis(connection_pool=pool)

    try:
        r.set(user_id, value)
        return True
    except:
        return False


def get_prop(pool, user_id, prop_name):
    r = redis.Redis(connection_pool=pool)

    value = r.get(str(user_id) + prop_name)
    if value is None:
        return None

    return value.decode()

def set_prop(pool, user_id, value, prop_name):
    r = redis.Redis(connection_pool=pool)

    try:
        r.set(str(user_id) + prop_name, value)
        return True
    except:
        return False