debug = True

import enum
class States(enum.Enum):
   INITIAL = '1'
   LOGIN = '2'
   SIGNUP = '3'
   TABLE_JOIN = '4'
   TEXT = '5'
   ACTION = '6'