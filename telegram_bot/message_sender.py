# 	FOR TESTING 
#-------------------------
import pika
import sys
import json
import time

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='player_queue_999_Max', exchange_type='fanout')

message = {                         
  "event": "preflop",                         
  "table_id": "123",                            
  "players": [                               
    {"position": 1, "username": "WillSmith", "stack": 1435}, 
    {"position": 3, "username": "RobertDowneyJr", "stack": 955},
    {"position": 5, "username": "MargotRobbie", "stack": 2530}
  ],                                                          
  "active_players": [1,3,5],                         
  "button_position": 3,                         
  "pot": 125,                         
  "current": {                                                           
    "cards": "AsJs"                          
  }                
}

channel.basic_publish(exchange='player_queue_999_Max', routing_key='player_queue_999_Max', body=json.dumps(message))



message = {        
  "event": "flop_cards",        
  "table_id": "105",        
  "flop_cards": ["5c", "Ts", "Qd"]
}

channel.basic_publish(exchange='player_queue_999_Max', routing_key='player_queue_999_Max', body=json.dumps(message))



message = {                
  "event": "turn_card",                
  "table_id": "123",                
  "turn_card": "Jh"
}

channel.basic_publish(exchange='player_queue_999_Max', routing_key='player_queue_999_Max', body=json.dumps(message))



message = {
    "event": "river_card",
    "table_id": "123",
    "river_card": "2c",
}

channel.basic_publish(exchange='player_queue_999_Max', routing_key='player_queue_999_Max', body=json.dumps(message))



message = {
    "event": "winner",
    "table_id": "555",
    "winner_position": 5,
    "pot": 150
 }

channel.basic_publish(exchange='player_queue_999_Max', routing_key='player_queue_999_Max', body=json.dumps(message))



message = {
  "event": "request_action",
  "table_id": "234",
  "username": "Neo", 
  "action_space": 
  [
    {"type": "fold"},
    {"type": "check"},
    {"type": "call", "size":20},
    {"type": "raise", "min": 50, "max": 150}
  ]
}

channel.basic_publish(exchange='player_queue_999_Max', routing_key='player_queue_999_Max', body=json.dumps(message))

connection.close()


