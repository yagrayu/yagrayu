# 	FOR TESTING 
#-------------------------
import pika
queue_name = "response_queue_999_Max"
connection = pika.BlockingConnection(
		pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange=queue_name, exchange_type='fanout')

result = channel.queue_declare(queue=queue_name, exclusive=True)


channel.queue_bind(exchange=queue_name, queue=queue_name)

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):
	print(body)

channel.basic_consume(
	queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()