.. yagrayu documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

yagrayu Project Documentation
====================================================================

Table of Contents:

.. automodule:: yagrayu
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices & Tables
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
